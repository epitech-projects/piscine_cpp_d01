/*
** white_rabbit.c for Follow the white rabbit in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d01/ex00
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Wed Jan  8 09:18:32 2014 Jean Gravier
** Last update Thu Jan  9 09:34:14 2014 Jean Gravier
*/

#include <stdlib.h>
#include <stdio.h>

int	follow_the_white_rabbit()
{
  int	number;
  int	sum;
  int	done;

  sum = 0;
  done = 0;
  while (!done)
    {
      number = (random() % 37) + 1;
      sum += number;
      if (number == 15 || number == 10 || number == 23)
	printf("devant\n");
      else if (number == 26 || !(number % 8))
	printf("derriere\n");
      else if ((number >= 4 && number <= 6) || (number>= 17 && number <= 21)
	       || number == 28)
	printf("gauche\n");
      else if (number == 13 || (number >= 34 && number != 37))
	printf("droite\n");
      if (number == 1 || number == 37 || sum >= 397)
	done = 1;
    }
  printf("LAPIN !!!\n");
  return (sum);
}
